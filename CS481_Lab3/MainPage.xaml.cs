﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms; 

namespace CS481_Lab3
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public static List<string> listThings = new List<string>();

        public MainPage()
        {
            InitializeComponent();

            //Amanda was here //

            listThings.Add("Amanda"); /*Referenced https://dzone.com/articles/xamarinforms-listview-with-pull-to-refresh/8 for Pull to Refresh */
            listThings.Add("Matt");
            listThings.Add("Josh");
            listThings.Add("Lorenzo");
            listThings.Add("Adam");
        }
        public void PulltoRefresh(object sender, EventArgs e)
            {
            doUpdate();
            List.EndRefresh();
        }
        public void doUpdate()
            {
                listThings.Add("TECHNO STARFISH!");
                List.ItemsSource = null;
                List.ItemsSource = listThings;
            }

        }
    }

